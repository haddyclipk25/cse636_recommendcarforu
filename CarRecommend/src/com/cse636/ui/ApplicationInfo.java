package com.cse636.ui;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

@Named
@ManagedBean
@Scope("session")
public class ApplicationInfo implements Serializable {
	
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
		private String slogan=" this is the project for cse636 ub fall 2016";
		private String budget=" Please tell us ur expected price";
		private String range="What range u can accept of your ideal price";
		private String country="What make u prefer (US, EU, JAPAN)";

		public String getBudget() {
			return budget;
		}

		public void setBudget(String budget) {
			this.budget = budget;
		}

		public String getRange() {
			return range;
		}

		public void setRange(String range) {
			this.range = range;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getSlogan() {
			return slogan;
		}

		public void setSlogan(String slogan) {
			this.slogan = slogan;
		}
		
		
}
