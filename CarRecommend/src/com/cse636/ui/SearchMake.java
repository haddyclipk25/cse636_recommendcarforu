package com.cse636.ui;



import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.cse636.dto.Country;
import com.cse636.dto.Dto;
import com.cse636.service.CarRecService;

@Named
@ManagedBean
@Scope("session")
public class SearchMake {
	
		/**
	 * 
	 */
	

		@Inject
		private Country country;
		
		@Inject
		private CarRecService carRecServiceImp;
		
		private List<Dto> cars;
		
		public List<Dto> getCars() {
			return cars;
		}

		public void setCars(List<Dto> cars) {
			this.cars = cars;
		}

		public String execute(){
		//	Dto dto=new Dto();
			//cars.add(dto);
			fetchCars("");
			if(country!=null && (country.getName().equalsIgnoreCase("japan")|| country.getName().equalsIgnoreCase("eu")||country.getName().equalsIgnoreCase("us"))){
			return "search";
			}
			else{
				return "noresults";
			}
		}
		
		List<Dto> fetchCars(String filter){
			cars=carRecServiceImp.filterCars(filter);
			return cars;
		}
		
		public Country getMakeCountry() {
			return country;
		}
		public void setMakeCountry(Country make) {
			this.country = make;
		}
		
		
		
}
