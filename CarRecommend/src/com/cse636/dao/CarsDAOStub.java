 package com.cse636.dao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import com.cse636.dto.Dto;

@Named("carsDAO")
public class CarsDAOStub implements ICarsDAO {

	@Override
	public List<Dto> fetchCars() {
		// TODO Auto-generated method stub
		List<Dto> cars=new ArrayList<Dto>();
		
		Driver driver=new Driver();
		List<List<String>> r=driver.retDB("23000", "2000");
		List<String> us=new ArrayList();
		
		for(List<String> car: r){
			Dto tmp=new Dto();
			tmp.setMake(car.get(1));
			tmp.setModel(car.get(2));
			tmp.setPrice(car.get(2));
			cars.add(tmp);
		}
		
		Dto ford1=new Dto();
		ford1.setMake("ford");
		ford1.setModel("fusion");
		
		Dto ford2=new Dto();
		ford2.setMake("ford");
		ford2.setModel("focus");
		
		Dto toyota1=new Dto();
		toyota1.setMake("toyota");
		toyota1.setModel("camary");
		
		Dto honda1=new Dto();
		ford1.setMake("honda");
		ford1.setModel("accord");
		
		cars.add(ford1);
		cars.add(ford2);
		cars.add(toyota1);
		cars.add(honda1);
		
		return cars;
	}

}
