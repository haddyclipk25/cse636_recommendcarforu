package com.cse636.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Driver {
	String maker; 
	String model; 
	String price; 
	String MinPrice; 
	String MaxPrice;
	
	
	static final String JDBC_Driver = "com.mysql.jdbc.Driver"; 
	static final String url = "jdbc:mysql://173.194.243.34"; 
	//:3306/CarRec
	static String user = "admin"; 
	static  String password = "admin"; 
	
	private  void createDB(){
		String createDB = "CREATE DATABASE CarRec; " + "USE CarRec; "; 
	}
	
	

	private static void insertRecordIntoTable() throws SQLException{
		Connection dbConnection = null; 
		PreparedStatement stmt = null; 
		String insertTableSQL = "INSERT INTO carRecommendation (Maker, Model, MinPrice, MaxPrice) VALUES('Toyota', 'Camry', '24000', '28000')";
						
		try{
			dbConnection = getDBConnection();
			stmt = dbConnection.prepareStatement(insertTableSQL);
			
			stmt.execute(insertTableSQL); 
			System.out.println("Record is inserted into DB table"); 
		}
		catch(SQLException e){
			System.out.println(e.getMessage()); 
		}
		finally{
			if(stmt != null){
				stmt.close(); 
			}
			if(dbConnection != null){
				dbConnection.close(); 
			}
		}
		System.out.println("Successfully inserted data into the database.");
	}
	
	public void initial(String useraccount, String password){
		user=useraccount;
		this.password=password;
		
		createDB();
		
		
	}

	private static Connection getDBConnection() {
		Connection dbConnection = null; 
		try{
			Class.forName(JDBC_Driver); 
		}
		catch(ClassNotFoundException e){
			System.out.println(e.getMessage()); 
		}
		try{
			dbConnection = DriverManager.getConnection(url, user, password); 
		}
		catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}
	

	public static List<List<String>> retDB(String price, String range){
		
		 List<List<String>> finalResult = new ArrayList();
		 int defaultRange=2000;
		 if(range!=null) defaultRange=Integer.valueOf(range);
		 
		 int p=Integer.valueOf(price);
		 
		 int min=p-defaultRange;
		 int max=p+defaultRange;
		
		try{
			Connection myConn = DriverManager.getConnection(url, user, password);
			Statement myStmt = myConn.createStatement(); 
			ResultSet rs = myStmt.executeQuery("select * from test.American;");
			
			ResultSet myRs = myStmt.executeQuery("select * from test.American WHERE MaxPrice < "+max+" AND MinPrice > "+ min + ";" );
			
			while (myRs.next()){
				ArrayList<String> dbRet = new ArrayList<>();
				dbRet.add(myRs.getString("Maker") + ", " + myRs.getString("Model") + ", " + myRs.getString("MinPrice") + ", " + myRs.getString("MaxPrice")); 
				finalResult.add(dbRet);
				System.out.println(myRs.getString("Maker") + ", " + myRs.getString("Model") + ", " + myRs.getString("MinPrice") + ", " + myRs.getString("MaxPrice")); 
			}
			
			//System.out.println("Processing Result ... " + ResultSet); 
		}
		catch(Exception exc){
			exc.printStackTrace(); 
		}
		
		return finalResult;
	}
	

}