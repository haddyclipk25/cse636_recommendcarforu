package com.cse636.dao;

import java.util.List;

import com.cse636.dto.Dto;

/**
 *  get Cars DAO from crawler
 * @author weihaoqu
 *
 */
public interface ICarsDAO {
	public List<Dto> fetchCars();
}
