package com.cse636.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.cse636.dao.ICarsDAO;
import com.cse636.dto.Dto;

/**
 * Implement the CarRecService interface
 * @author weihaoqu
 *
 */

@Named
public class CarRecServiceImp implements CarRecService {

	@Inject
	private ICarsDAO carsDAO;
	
	private List<Dto> allCars;
	
	@Override
	public List<Dto> filterCars(String filter) {
		// TODO Auto-generated method stub
		 allCars=  getCarsDAO().fetchCars();
		 
		 List<Dto> returnCars=new ArrayList<Dto>();
		 
		 for(Dto dto: allCars){
			 returnCars.add(dto);
		 }
		
		return returnCars;
	}

	public ICarsDAO getCarsDAO() {
		return carsDAO;
	}

	public void setCarsDAO(ICarsDAO carsDAO) {
		this.carsDAO = carsDAO;
	}

}
