package com.cse636.service;

import java.util.List;

import com.cse636.dto.Dto;

/**
 *  recommend a list of carsDto based on the filter
 * @author weihaoqu
 *
 */

public interface CarRecService {
	List<Dto> filterCars(String filter);
		
	
}
